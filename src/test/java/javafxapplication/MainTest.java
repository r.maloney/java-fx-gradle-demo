package javafxapplication;

import javafx.scene.Parent;
import javafx.scene.input.KeyCode;
import javafxapplication.MainNode;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.loadui.testfx.GuiTest;

public class MainTest extends GuiTest {
   public Parent getRootNode() {
      return new MainNode();
   }

//   @After
//   public void teardown() {
//       closeCurrentWindow();
//   }

   @Category(javafxapplication.UserAcceptanceTest.class)
   @Test
   public void testMainNode() throws InterruptedException {
       Thread.sleep(3000);
       push(KeyCode.CONTROL, KeyCode.A);
       closeCurrentWindow();
   }
}